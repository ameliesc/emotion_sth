import os
import logging
logging.getLogger('tensorflow').setLevel(logging.INFO)
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
import tensorflow as tf
from sklearn.decomposition import PCA
from pathos.multiprocessing import ProcessingPool as Pool
from math import floor
from collections import OrderedDict
from tqdm import tqdm
from read_tfrecord import load_data, create_c3d_layer_dict
from p_tqdm import p_map
from sklearn.model_selection import KFold
import numpy as np
import _pickle as pkl
import scipy


__output_dir__ = '/data1/amelie/EnD/regr_output/'
#__train_size__ = 0.85
def load_general_data(layer,net, data_pipeline,response,fpf):
    dataset = load_data(__filename__,layer, 1, 1, __train_size__, mode = "train", net = net, data_pipeline = data_pipeline,response = response,fpf = fpf) ## removwe mode from load function, niot relevant for our dataset 
    eval_dataset = load_data(__filename__,layer, 1, 1, __train_size__, mode = "eval",  net = net, data_pipeline = data_pipeline,response =response,fpf = fpf)
    return dataset,eval_dataset



def run_data(dataset,sess):
    iterator = dataset.make_one_shot_iterator()
    next_element = iterator.get_next() #c3d_dict, main_data, mimic_data, aourasl ,valence
    c3d = next_element[0]
    
    
    
    fmri_data= next_element[1]
    
    
    #print("Loading kfold data...")
    c3d_list = []
    voxel_list = []
    ##Batch size should be 1
    #sess = tf.Session()
    #sess.run(tf.initialize_all_variables())
    
    #import pdb; pdb.set_trace()
    
    while True:
        try: ## need to load data like this because we have to use scikit for crossval ... :( so slow
        ## mskr yhid s seperate function, load all voxels once, and just call different one depending on loop
            
            
            x, y = sess.run([c3d,fmri_data])
            x.reshape([1, -1])
            
            # return to stuff
            

            c3d_list.append(x)
            voxel_list.append(y)
        except:
            break
    c3d = np.concatenate(c3d_list, axis = 0)
    voxel1 = np.concatenate(voxel_list, axis = 0)
    
    #print("Done.")
    #sess.close()
    return np.squeeze(c3d), np.squeeze(voxel1)


class RidgeRegression(object):
    def __init__(self,n_components, n_voxels,session ):
        self.n_components = n_components[0]
        self.n_voxels = n_voxels
        self.sess = session

    def graph(self):
        
        
        self.feat = tf.placeholder(tf.float32,shape = (None, self.n_components))
        self.lmbda = tf.placeholder(tf.float32,shape = (None))
        self.voxels = tf.placeholder(tf.float32,shape = (None,self.n_voxels))
        self.i = tf.placeholder(tf.int16, shape = [None])
        self.W_pl = tf.placeholder(tf.float32,shape = (self.n_components, self.n_voxels))
        C = tf.matmul(tf.transpose(self.feat),self.feat) + self.lmbda *tf.eye(self.n_components)
        self.W_graph = tf.squeeze(tf.matmul(tf.linalg.inv(C),tf.matmul(tf.transpose(self.feat),self.voxels)))
        self.y_pred_graph = tf.matmul(self.feat,self.W_pl)
        
        
        #self.pearson_r = tf.contrib.metrics.streaming_pearson_correlation(tf.reshape(self.y_pred_graph[:,self.i], [-1]),tf.reshape(self.voxels[:,self.i], [-1]))
    def fit(self,lmbda,x,y):
        self.W = self.sess.run(self.W_graph, feed_dict = {self.feat: x, self.lmbda : lmbda, self.voxels : y})
        self.W = np.squeeze(self.W)
    def get_w(self):
        return self.W

    def predict(self,x):
        y_pred = self.sess.run(self.y_pred_graph, feed_dict = {self.feat: x, self.W_pl : self.W} )
        return y_pred


    def kfold_cross_val(self,x,y,lmbda, n_splits):
        kf = KFold(n_splits = n_splits)
        j = 0
        voxel_cost = np.zeros((y.shape[1], n_splits))
        voxel_p_val =  np.zeros((y.shape[1], n_splits))
        for train_index, test_index in tqdm(kf.split(x)):
            x_train, x_test = x[train_index], x[test_index]
            y_train, y_test = y[train_index], y[test_index]
            
            self.fit(lmbda,x_train,y_train)
       
    
            for i in range(y.shape[1]):
                y_pred_t = self.predict(x_test)

                
                pearson_r, p_value =  scipy.stats.pearsonr(np.asarray(y_pred_t[:,i]).reshape(-1),np.asarray(y_test[:,i]).reshape(-1))
                #pearson_r = self.sess.run(self.pearson_r, feed_dict = {self.feat : x_test, self.voxels : y_test, self.i : i})
                voxel_cost[i,j] = pearson_r
                voxel_p_val[i,j] = p_value
            j += 1
        voxel_cost = np.mean(voxel_cost, axis = 1)
        voxel_p_val = np.mean(voxel_p_val,axis = 1)
        assert voxel_cost.shape[0] == y.shape[1]
        return voxel_cost,voxel_p_val

    
    def main(self,x,y,x_eval,y_eval):
        lmbda_list = np.logspace(-7,2,num = 9, base = 10, dtype = 'float32').tolist()
    
        i = 0
        #lmbda_list = [1]
        lmbda_error = np.zeros((len(lmbda_list),y.shape[1]))
        lmbda_p_value = np.zeros((len(lmbda_list),y.shape[1]))
        pred_error = np.zeros((y.shape[1]))
        p_value = np.zeros((y.shape[1]))
        l = len(lmbda_list)
        for lmbda in lmbda_list:
            
            y_cost,p_value = self.kfold_cross_val(x,y,lmbda,7)
            lmbda_error[i] = y_cost
            lmbda_p_value[i] = p_value
            i +=1
        
            
        lmbda_index = np.argmax(np.abs(lmbda_error), axis = 0).tolist()
        
        

            
        y_pred = np.zeros(y_eval.shape)
        w = np.zeros((x.shape[1],y_eval.shape[1]))
        for i in tqdm(range(y.shape[1])):
         pred_error[i] = lmbda_error[lmbda_index[i]][i]
         p_value[i] = lmbda_p_value[lmbda_index[i]][i]
         lmbda = lmbda_list[lmbda_index[i]]
         self.fit(lmbda,x,y)
         y_pred_all = self.predict(x_eval)
         y_pred[:,i] = y_pred_all[:,i]
         w[:,i] = self.W[:,i]



        return y_pred,y_eval,w,pred_error, p_value





if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description= __doc__)
    #parser.add_argument('--voxel', type = int, required = True)
    parser.add_argument('--layer')
    parser.add_argument('--net', type = str, required = True, choices = ['c3d', 'alexnet'])
    parser.add_argument('--data_pipeline', type = str, required = True ,  choices = ['movie_pca', 'serialized' ])
    parser.add_argument('--response', type = str, required = True, choices = ['mimic', 'main'])
    parser.add_argument('--fpf', type = str,  choices = ['fpf15', 'fpf1', 'fpf1-2'], default = '')

    parser.add_argument('--dir_name', type= str, required = True)
    parser.add_argument('--subject', type=str)
    parser.add_argument('--train_size', type = float, required = True )
    parser.add_argument('--pca_mode',type=str, required = True)
    
    args = parser.parse_args()
    __train_size__ = args.train_size
    print(args.subject)
    if args.pca_mode == 'new_pca':
        __filename__ = '/data1/amelie/EnD/tfrecord/time_series_brain_rsp/new_pca/subject' + args.subject + '/'
        #__filename0__ = '/data1/amelie/EnD/tfrecord/time_series_brain_rsp/new_pca/'
    elif args.pca_mode == 'old':
        __filename__ = '/data1/amelie/EnD/tfrecord/time_series_brain_rsp/old/subject' + args.subject + '/'
    if len(args.fpf) > 2:

        filename_dims = os.path.join(__filename__,args.data_pipeline + '_'+ args.net +'_'+ args.response + '_' + args.fpf +'_dim.txt')
    else:
        filename_dims = os.path.join(__filename__,args.data_pipeline + '_'+ args.net +'_'+ args.response  +'_dim.txt')
    #print(filename_dims)
    #import pdb; pdb.set_trace()

    assert(os.path.exists(filename_dims))
    output_path =os.path.join(__output_dir__, args.pca_mode)
    if not os.path.exists(output_path):
        os.mkdir(output_path)
    output_path =os.path.join(output_path, args.dir_name)
    if not os.path.exists(output_path):
        os.mkdir(output_path)
    layer_dict,_ = create_c3d_layer_dict(filename_dims)
    
    layer_dict2, _ = create_c3d_layer_dict(filename_dims)
    dataset, dataset_eval = load_general_data(args.layer, args.net, args.data_pipeline,args.response,args.fpf)
    session = tf.Session()
    regr = RidgeRegression(layer_dict[args.layer], 20005,session)
    regr.graph()
    c3d, voxel1 = run_data(dataset,session)
    
    c3d_eval, voxel1_eval = run_data(dataset_eval,session)
    y_pred,y_true, w,pred_error, p_value = regr.main(c3d,voxel1, c3d_eval,voxel1_eval)

   
    predictions_filename =os.path.join(output_path,'{}_{}_y1_eval_predictions.pkl'.format(args.layer,args.response))
    true_predictions_filename =  os.path.join(output_path,'{}_{}_y1_eval_true.pkl'.format(args.layer,args.response))
    weights_filename = os.path.join(output_path,'{}_{}_weights.pkl'.format(args.layer,args.response))
    error_filename = os.path.join(output_path,'{}_{}_error.pkl'.format(args.layer,args.response))
    pvalue_filename = os.path.join(output_path,'{}_{}_pvalue.pkl'.format(args.layer,args.response))
    print("finished encoding model, saving predictions and weights")
    with open(predictions_filename, 'wb') as f:
        pkl.dump(y_pred,f,protocol =4)
    with open(weights_filename, 'wb') as f:
        pkl.dump(w,f,protocol =4)
    with open(true_predictions_filename,'wb') as f:
        pkl.dump(y_true,f,protocol =4)
    with open(error_filename,'wb') as f:
        pkl.dump(pred_error,f,protocol =4)
    with open(pvalue_filename,'wb') as f:
        pkl.dump(p_value,f,protocol =4)


    session.close()
