from get_c3d_feat import main
import glob
import os

# c3d layer list

layer_list = ["conv1a", "conv2a", "conv3a", "conv3b", "conv4a", "conv4b", "conv5a", "conv5b", "fc6-1", "fc7-1","fc8-1"]


layer_dim_list = [64,128,256,256,512,512,512,512,4096,4096]

#layer_list = ["conv5b"]
#layer_dim_list = [512]

frm_directory = "/data1/amelie/EnD/frm_time_series/frm20/"

feat_dir = "/data1/amelie/EnD/c3d/c3d_run/fpf1/"

for layer, layer_dim in zip(layer_list, layer_dim_list):
    video_list = glob.glob(frm_directory + "*")
    #import pdb; pdb.set_trace()
    
    video_list = [x.split("/")[-1] for x in video_list]
    #import pdb; pdb.set_trace()

    for video in video_list:
        main(video, frm_directory,os.path.join(feat_dir,video),feature_layer = layer,c3d_length = layer_dim)
