import cv2
import os
import sys
import glob
import numpy as np
#import cPickle as pickle

__video_width__  = 400.0

def extract_frames(videoclip_dir,frame_dir):
    if not os.path.exists(frame_dir):
        os.mkdir(frame_dir)
    video_clip_list = glob.glob(videoclip_dir + "/*.mov")
    
    #import pdb; pdb.set_trace()
    
    assert len(video_clip_list) == 6
    for videoclip in video_clip_list:
        vidcap = cv2.VideoCapture(videoclip)
        #import pdb; pdb.set_trace()
        fps = vidcap.get(cv2.CAP_PROP_FPS)
        #print(fps)
        vidcap.set(cv2.CAP_PROP_FPS, 30.0)
        #continue
        #vidcap.set(cv2.CAP_PROP_FPS, 24) ## Need same number of frames per video, so sseteting to lowest common fps
        #n_frames = 
        success,image = vidcap.read()
        video_name = videoclip.split('/')[-1].split('.')[0]
        count = 1 ## start with 1 because of c3d feat extraction
        video_clip_frame_dir = os.path.join(frame_dir,video_name)
        video_clip_frame_dir_resized =  os.path.join(frame_dir,video_name + "_resized")
        #if not os.path.exists(video_clip_frame_dir):
        #    os.mkdir(video_clip_frame_dir)
        #if not os.path.exists(video_clip_frame_dir_resized):
        #x    os.mkdir(video_clip_frame_dir_resized)
        print("extracting frames for %s" % video_name)
        j = 0
        while success:
            #if count == 1201:
            #    for i in range(15):
            #        cv2.imwrite(os.path.join(new_dir,"%06d.jpg" % count), image)
            #        count +=1
            #    count = 1201
            success,image = vidcap.read()
            if success == False:
                break
            #if count ==  1201:
                
            #   j += 1
            #   count = 1
            new_dir = video_clip_frame_dir # + '_{}'.format(j)
            
            if not os.path.exists(new_dir):
               os.mkdir(new_dir)
            
            cv2.imwrite(os.path.join(new_dir,"%06d.jpg" % count), image)     # save frame as JPEG file
            #if not success:
            #    break
            # height = int(vidcap.get(cv2.CAP_PROP_FRAME_HEIGHT))
            # width = int(vidcap.get(cv2.CAP_PROP_FRAME_WIDTH))
            # scale = __video_width__ / width
            # new_height = int(scale * height)
            # new_width = int(scale * width)
            # #frames_n = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
            # frame = cv2.resize(image, (new_width,new_height))
            # cv2.imwrite(os.path.join(video_clip_frame_dir_resized,"%06d.jpg" % count), frame)
            #print('Read a new frame: ', success)
            count += 1
        print(count)


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description= __doc__)
    parser.add_argument('--video_dir', type=str)
    parser.add_argument('--frame_dir', type=str)

    args = parser.parse_args()

    extract_frames(args.video_dir,args.frame_dir)
