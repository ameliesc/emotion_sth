import numpy as np
import tensorflow as tf
from scipy import io
import glob
import os
labels_path = '/data1/amelie/EnD/object_classes'
labels_file = 'object.mat'
frame_path = '/data1/amelie/EnD/frm_uncropped'
object_mat = io.loadmat(os.path.join(labels_path, labels_file))

run1 = object_mat['object'][0][0][3][0][0]  # (926,390) -> (class, sample)
run2 = object_mat['object'][0][0][3][1][0]
run3 = object_mat['object'][0][0][3][2][0]
input_txt = 'frm_labels_input_3.txt'

for run_path in sorted(glob.glob(os.path.join(frame_path, '*'))):
    # if 'Ctr' in run_path:
    #     i = 0
    #     test = 0
    #     for frame_path in sorted(glob.glob(os.path.join(run_path, '*'))):
    #         if i % 30 == 0:
    #             test += 1
    #             with open(input_txt, 'a') as f:
    #                 label=-1
    #                 f.write(frame_path + ' ' + str(label) + ' ' '\n')

                

    #         i += 1
        
        
    #     assert(test == run1.shape[1])


    if 'Run1_' in run_path:
        i = 0
        k=0
        for frame_path in sorted(glob.glob(os.path.join(run_path, '*'))):
            if i % 30 == 0:

                with open(input_txt, 'a') as f:
                    label=np.where(run1[:, k] == 1)
                    f.write(frame_path + ' ')
                    for l in label[0]:
                        
                        
                        f.write(str(l) + ' ')
                    f.write('\n')
                k += 1

            i += 1
        assert(k == run1.shape[1])

    elif 'Run2_' in run_path:
        i=0
        k=0
        for frame_path in sorted(glob.glob(os.path.join(run_path, '*'))):
            if i % 30 == 0:

                with open(input_txt, 'a') as f:
                    label=np.where(run2[:, k] == 1)
                    f.write(frame_path + ' ')
                    for l in label[0]:
                        f.write(str(l) + ' ')
                    f.write('\n')

                k += 1

            i += 1
        assert(k == run1.shape[1])

    elif 'Run3_' in run_path:
        i=0
        k=0
        for frame_path in sorted(glob.glob(os.path.join(run_path, '*'))):
            if i % 30 == 0:

                with open(input_txt, 'a') as f:
                    label=np.where(run3[:, k] == 1)
                    f.write(frame_path + ' ')
                    for l in label[0]:
                        f.write(str(l) + ' ')
                    f.write('\n')

        
                k += 1

            i += 1
        assert(k == run1.shape[1])
