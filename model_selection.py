import os
import logging
logging.getLogger('tensorflow').setLevel(logging.INFO)
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
import tensorflow as tf
from sklearn.decomposition import PCA
from pathos.multiprocessing import ProcessingPool as Pool
from math import floor
from linear_regression3 import RidgeRegression
from collections import OrderedDict
from tqdm import tqdm
from read_tfrecord import load_data, create_c3d_layer_dict
from p_tqdm import p_map
from scipy import io
from sklearn.model_selection import KFold
import numpy as np
import _pickle as pkl
import scipy

__train_size__ = 0.8
def load_general_data(__filename__,layer,net, data_pipeline,response,fpf):
    dataset = load_data(__filename__,layer, 1, 1, __train_size__, mode = "train", net = net, data_pipeline = data_pipeline,response = response,fpf = fpf) ## removwe mode from load function, niot relevant for our dataset 
    eval_dataset = load_data(__filename__,layer, 1, 1, __train_size__, mode = "eval",  net = net, data_pipeline = data_pipeline,response =response,fpf = fpf)
    return dataset,eval_dataset

def run_data(dataset,sess):
    iterator = dataset.make_one_shot_iterator()
    next_element = iterator.get_next() #c3d_dict, main_data, mimic_data, aourasl ,valence
    c3d = next_element[0]
    
    
    
    fmri_data= next_element[1]
    
    
    #print("Loading kfold data...")
    c3d_list = []
    voxel_list = []

    while True:
        try:
            
            x, y = sess.run([c3d,fmri_data])
            x.reshape([1, -1])
            

            c3d_list.append(x)
            voxel_list.append(y)
        except:
            break
    c3d = np.concatenate(c3d_list, axis = 0)
    voxel1 = np.concatenate(voxel_list, axis = 0)

    return np.squeeze(c3d), np.squeeze(voxel1)

def unison_shuffled_copies(a, b):
    assert len(a) == len(b)
    p = np.random.permutation(len(a))
    return a[p], b[p]

def get_pearson_scores(y_pred,y_test):
    pearson_r_all = np.zeros((y_pred.shape[1]))
    p_value_all = np.zeros((y_pred.shape[1]))
    for i in range(y_pred.shape[1]):
        
        pearson_r, p_value =  scipy.stats.pearsonr(np.asarray(y_pred[:,i]).reshape(-1),np.asarray(y_test[:,i]).reshape(-1))
        pearson_r_all[i] = pearson_r
        p_value_all[i] = p_value

    return pearson_r_all, p_value_all


def layer_selection(subject,data_pipeline, net,response,fpf,p_value):
    __results_dir__ = '/data1/amelie/EnD/regr_output/old/subject' + str(subject) + '/fixed' #+'/' +fpf
    __filename__ = '/data1/amelie/EnD/tfrecord/time_series_brain_rsp/old/subject'+ str(subject)
    __mat__ = '/data1/amelie/EnD/fmri_time_series/CUBEE%04d_ts_GM.mat' % (subject +1)
    
    filename_dims = os.path.join(__filename__,data_pipeline + '_'+ net +'_'+ response + '_' + fpf +'_dim.txt')
    layer_dict,_ = create_c3d_layer_dict(filename_dims)
    
        
    layer_dict2,_= create_c3d_layer_dict(filename_dims)
    layer_pearson_cross = np.zeros((len(layer_dict.keys()),20005))
    layer_p_value_cross  = np.zeros((len(layer_dict.keys()),20005))
    layer_pearson = np.zeros((len(layer_dict.keys()),20005))
    layer_p_value = np.zeros((len(layer_dict.keys()),20005))
    i = 0
    p_threshhold = p_value
    main_results = {}
    main_results_file = "{}_{}_{}_encoding_model_results_p_value_{}".format(net,fpf,response,p_value)
    
    for layer in layer_dict.keys():
        #if 'conv3' not in layer:
        #    continue
        tf.reset_default_graph()
        dataset, dataset_eval = load_general_data(__filename__,layer, args.net, args.data_pipeline,args.response,args.fpf)
        session = tf.Session()
        c3d, voxel1 = run_data(dataset,session)
    
        c3d_eval, voxel1_eval = run_data(dataset_eval,session)
        if layer == '':
            continue
        predictions_main_filename = '{}_{}_y1_eval_predictions.pkl'.format(layer,response)
        true_predictions_main_filename = '{}_{}_y1_eval_true.pkl'.format(layer,response)
        weights_main_filename = '{}_{}_weights.pkl'.format(layer,response)
        error_main_filename = '{}_{}_error.pkl'.format(layer,response)
        pvalue_main_filename = '{}_{}_pvalue.pkl'.format(layer,response)
        with open(os.path.join(__results_dir__, weights_main_filename), 'rb') as f:
            weights = pkl.load(f)
        with open(os.path.join(__results_dir__,predictions_main_filename), 'rb') as f:
            y_pred = pkl.load(f)
        with open(os.path.join(__results_dir__,true_predictions_main_filename), 'rb') as f:
            y_pred_true = pkl.load(f)
        with open(os.path.join(__results_dir__,error_main_filename), 'rb') as f:
            error  = pkl.load(f)
        with open(os.path.join(__results_dir__,pvalue_main_filename), 'rb') as f:
            pvalue  = pkl.load(f)


            
            
        pearson_r_all, p_value_all = get_pearson_scores(y_pred,y_pred_true)
        
        
        layer_pearson_cross[i] = np.abs(error) ## error from cross prediction
        layer_p_value_cross[i] = pvalue
        layer_pearson[i] = pearson_r_all ## error from cross prediction
        layer_p_value[i] = p_value_all
        i +=1


    #### layer selections
    ### discard voxels where p_value is above threshold
    #`    import pdb; pdb.set_trace()


    #import pdb; pdb.set_trace()
    
    assigned_layer = np.argmax(layer_pearson_cross, axis = 0)
    assert len(assigned_layer) == 20005
    voxel_id_list = []
    layer_id_list = []
    pearson_r_val_list = []
    p_value_list = []
    coord_list = []
    mat = io.loadmat(__mat__)
    voxel_id = np.where(layer_p_value > p_value) ## discard all the voxels whose value is above a certain p_value
    layer_pearson[voxel_id] = 0
    coord = mat['DataMat']['GMcoordinate'][0][0]


    


    for i in range(20005):
        if layer_pearson[assigned_layer[i]][i] == 0:
            continue
        else:
            voxel_id_list.append(i)
            layer_id_list.append(assigned_layer[i])
            pearson_r_val_list.append(layer_pearson[assigned_layer[i]][i])
            p_value_list.append(layer_p_value[assigned_layer[i]][i])
            coord_list.append(coord[i])
    main_results['voxel_id'] = np.array(voxel_id_list)
    main_results['layer_id'] = np.array(layer_id_list)
    main_results['pearson_r'] = np.array(pearson_r_val_list)
    main_results['p_value'] =  np.array(p_value_list)
    main_results['coord'] = np.array(coord_list)
    
    #import pdb; pdb.set_trace()
    #import pdb; pdb.set_trace()

    #with open(main_results_file, 'wb') as f:
    #    
    #    pkl.dump(main_results,f,protocol = 4)
    print("saving results at {}".format(os.path.join(__results_dir__,main_results_file)))
    scipy.io.savemat(os.path.join(__results_dir__,main_results_file), mdict = main_results)

    
    
    ## Niftifile
    example_filename = '/data/amelie/emotioncap/GMmask_templateCUBE.nii'

    import nibabel as nib
    img = nib.load(example_filename)
    brain_volume = np.zeros((84,84,46))
    #import pdb; pdb.set_trace()
    
    j = 0
    coord  = main_results['coord'] 
    
    
    for i in range(20005): 
        if i in main_results['voxel_id']:
            
            
            
            brain_volume[coord[j][0]][coord[j][1]][coord[j][2]] =  1#main_results['pearson_r'][j]
            j +=1

    #import pdb; pdb.set_trace()
    
    new_img = nib.Nifti1Image(brain_volume, img.affine)
    new_img.to_filename(os.path.join(__results_dir__,'voxel_id_'+ response + '_{}.nii.gz'.format(p_value)))
    

    #new_img = nib.Nifti1Image(main_results['pearson_r'], img.affine)
    #new_img.to_filename(os.path.join(__results_dir__,'pearson_r'+ response +'.nii.gz'))
    
if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description= __doc__)
    #parser.add_argument('--voxel', type = int, required = True)
    parser.add_argument('--net', type = str, required = True, choices = ['c3d', 'alexnet'])
    parser.add_argument('--data_pipeline', type = str, required = True ,  choices = ['movie_pca', 'serialized' ])
    parser.add_argument('--response', type = str, required = True, choices = ['mimic', 'main'])
    parser.add_argument('--fpf', type = str,  choices = ['fpf15', 'fpf1'], default = '')
    parser.add_argument('--p_value', type = float, required = True)
    args = parser.parse_args()

    for subject in [0,3,58,14,62]:
        layer_selection(subject,args.data_pipeline,args.net,args.response,args.fpf,args.p_value)
    

