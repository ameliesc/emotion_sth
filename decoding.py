import os
import logging
logging.getLogger('tensorflow').setLevel(logging.INFO)
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
import tensorflow as tf
from sklearn.metrics import hamming_loss, zero_one_loss, average_precision_score
from sklearn.model_selection import GridSearchCV
from sklearn.decomposition import PCA
from pathos.multiprocessing import ProcessingPool as Pool
from math import floor
from collections import OrderedDict
from tqdm import tqdm
from read_tfrecord import load_data, create_c3d_layer_dict
from p_tqdm import p_map
from sklearn.model_selection import KFold
import numpy as np
import _pickle as pkl
import scipy
import sklearn


__filename__ = '/data1/amelie/EnD/tfrecord/time_series_brain_rsp/subject3/'
__output_dir__ = '/data1/amelie/EnD/regr_output/'

__filename_main__ = '/data1/amelie/EnD/regr_output/train90/fpf15/subject3/alexnet_fpf15_main_encoding_model_results_p_value_0.01.mat'


def load_general_data(layer,net, data_pipeline,response,fpf):
    dataset = load_data(__filename__,layer, 1, 1, __train_size__, mode = "train", net = net, data_pipeline = data_pipeline,response = response,fpf = fpf) ## removwe mode from load function, niot relevant for our dataset 
    eval_dataset = load_data(__filename__,layer, 1, 1, __train_size__, mode = "eval",  net = net, data_pipeline = data_pipeline,response =response,fpf = fpf)
    return dataset,eval_dataset



def run_data(dataset,sess):
    iterator = dataset.make_one_shot_iterator()
    next_element = iterator.get_next() #c3d_dict, main_data, mimic_data, aourasl ,valence
    c3d = next_element[0]
    
    
    
    fmri_data= next_element[1]
    
    arousal = next_element[2]
    valence = next_element[3]
    c3d_list = []
    voxel_list = []
    arousal_list = []
    valence_list = []
    while True:
        try:
            
            
            x, y,w,z = sess.run([c3d,fmri_data, arousal, valence])
        
            x.reshape([1, -1])
            
            # return to stuff
            
            arousal_list.append(w)
            valence_list.append(z)
            c3d_list.append(x)
            voxel_list.append(y)
        except:
            break
    c3d = np.concatenate(c3d_list, axis = 0)
    voxel1 = np.concatenate(voxel_list, axis = 0)
    arousal = np.concatenate(arousal_list, axis = 0)
    valence  = np.concatenate(valence_list, axis = 0)
    #print("Done.")
    #sess.close()
    return np.squeeze(c3d), np.squeeze(voxel1), np.squeeze(arousal), np.squeeze(valence)







def svm(X,Y, X_eval, Y_eval):
    tuned_parameters = [
    {'C': np.logspace(-11,3, num = 15,base = 10), 'kernel': ['linear']}]
    X = np.squeeze(X)
    Y = np.squeeze(Y)
    X_eval = np.squeeze(X_eval)
    Y_eval = np.squeeze(Y_eval)
    clf = GridSearchCV(sklearn.svm.SVR(), tuned_parameters, cv=9,verbose = 3, n_jobs = 9)
    print("[INFO] Starting grid search...")
    import pdb; pdb.set_trace()

    clf.fit(X, Y)
    print("[INFO] Done.")
    print(clf.best_params_)
    C = clf.best_params_["C"]
    svm  = sklearn.svm.SVR(C =C) #SVC(kernel = 'linear', C=10)
    print("[INFO] Fitting model ...")
    import pdb; pdb.set_trace()

    svm.fit(X,Y)
    print("[INFO] Done.")
    pred = svm.predict(X_eval)
    pearson_r, p_value =  scipy.stats.pearsonr(np.asarray(pred).reshape(-1),np.asarray(Y_eval).reshape(-1))
    #accuracy = np.sum(pred == Y)/np.asarray(len(Y), dtype = np.float32)
    print("Mean average precision:", accuracy)
    print("Pearson:", pearson_r)


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description= __doc__)
    #parser.add_argument('--voxel', type = int, required = True)
    parser.add_argument('--layer')
    parser.add_argument('--net', type = str, required = True, choices = ['c3d', 'alexnet'])
    parser.add_argument('--data_pipeline', type = str, required = True ,  choices = ['movie_pca', 'serialized' ])
    parser.add_argument('--response', type = str, required = True, choices = ['mimic', 'main'])
    parser.add_argument('--fpf', type = str,  choices = ['fpf15', 'fpf1'], default = '')

    parser.add_argument('--train_size', type = float )


    args = parser.parse_args()
    __train_size__ = args.train_size 
    
    if len(args.fpf) > 2:
        filename_dims = os.path.join(__filename__,args.data_pipeline + '_'+ args.net +'_'+ args.response + '_' + args.fpf +'_dim.txt')
    else:
        filename_dims = os.path.join(__filename__,args.data_pipeline + '_'+ args.net +'_'+ args.response  +'_dim.txt')

    layer_dict,_ = create_c3d_layer_dict(filename_dims)
    
    layer_dict2, _ = create_c3d_layer_dict(filename_dims)

    dataset, dataset_eval = load_general_data(args.layer, args.net, args.data_pipeline,args.response,args.fpf)

        
    session = tf.Session()
    results = scipy.io.loadmat(__filename_main__)
    voxel_id = results['voxel_id']
    c3d, voxel1, arousal,valence = run_data(dataset,session)
    c3d_eval, voxel1_eval, arousal_eval,valence_eval = run_data(dataset_eval,session)
    
    import pdb; pdb.set_trace()
        ## select voxels that we were able to predict,
        ## check arousal shape, probably wrong shape for prediction
    #svm(voxel1[:,, arousal, voxel1_eval[:,voxel_id], arousal_eval)
    svm(c3d, arousal, c3d_eval, arousal_eval)
