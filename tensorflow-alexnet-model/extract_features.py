from alexnet import AlexNet
from tqdm import tqdm
import tensorflow as tf
import numpy as np
import glob
import os
import _pickle as pkl
import cv2


import argparse
parser = argparse.ArgumentParser(description=__doc__)
parser.add_argument('--fps', type = str, choices = ['fpf15_2', 'fpf1', 'fpf1-2'])

args = parser.parse_args()
## args : c3d, alexnet
## change name of tfrecord accordingky

#layer_name_list = ['conv1','conv2', 'conv3', 'conv4', 'conv5', 'fc6', 'fc7']
layer_name_list = ['fc8']

__feat_data_root__ = '/data1/amelie/EnD/alexnet/alexnet_run/' + args.fps
if args.fps == 'fpf1-2':
    __frm_data_root__ = '/data1/amelie/EnD/frm_time_series/frm20/'
else:
     __frm_data_root__ = '/data1/amelie/EnD/frm_time_series'
def create_feature_dic():
    feature_dic = {}
    for name in layer_name_list:
        feature_dic[name] = []
    return feature_dic

if args.fps == 'fpf15_2':
    x = tf.placeholder(tf.float32, [15,227,227,3])#img dim,  ###
elif args.fps == 'fpf1-2':
    x = tf.placeholder(tf.float32, [1,227,227,3])
else:
    x = tf.placeholder(tf.float32, [1,227,227,3])#img dim,  ### 
num_classes = 1000 #doesn't matter because we arent training
keep_prob = tf.placeholder(tf.float32)
skip_layer = []
weights_path = '/data/amelie/alexnet/tensorflow-alexnet-model/bvlc_alexnet.npy'
model = AlexNet(x,keep_prob,num_classes,skip_layer,weights_path) 

conv1 = model.conv1 
conv2 = model.conv2 
conv3 = model.conv3 
conv4 = model.conv4 
conv5 = model.conv5
fc6 = model.fc6
fc7 = model.fc7
fc8 = model.fc8
init = tf.initialize_all_variables()
sess = tf.Session()
sess.run(init)
feature_dic = create_feature_dic()

movie_clips_path  = glob.glob(os.path.join(__frm_data_root__, '*'))
ret = model.load_initial_weights(sess)
mu = np.load( './ilsvrc_2012_mean.npy')
mu = mu[:,15:242,15:242]  # the mean (BGR) pixel values
mu = np.moveaxis(mu,0,-1).astype(np.float32)
#import pdb; pdb.set_trace()


for movie_path in movie_clips_path:
    #import pdb; pdb.set_trace()

    movie_name = movie_path.split('/')[-1]
    feat_path = os.path.join(__feat_data_root__ , movie_name)

    
    if len(glob.glob(feat_path +'/*')) == 7:
        continue

    sess.run(init)

    print("Extracting features for {} ...".format(movie_name))
    frame_list = glob.glob(os.path.join(movie_path, '*.jpg'))
    feature_dic = create_feature_dic()
    i = 0
    pbar = tqdm(total=len(frame_list) - 15)
    
    
    while i < len(frame_list)-15: # last 15 frames are dummy frames!!
        frame = frame_list[i]
        if args.fps == "fpf15_2":
            
            pbar.update(5)
        else:
            pbar.update(1)
        
            
        
        if args.fps == "fpf15_2":
            img_data = cv2.imread(frame,cv2.IMREAD_COLOR)
            
            img_data = cv2.normalize(img_data,None,0,255,cv2.NORM_MINMAX)
            #img = np.moveaxis(imh)
            
            img_data = cv2.resize(img_data,(227,227))
            
            
            
            img_data = img_data.astype(np.float32)
            
            img_data = img_data - mu
            
            # height, width, depth = oriimg.shape
            # imgScale = W/width
            # newX,newY = oriimg.shape[1]*imgScale, oriimg.shape[0]*imgScale
            #try:
    
            #except: import pdb; pdb.set_trace()
            img_data = np.expand_dims(img_data,axis = 0)
            for j in range(1,15): ##geth 15second long feature clip
                frame = frame_list[i + j]
                img = cv2.imread(frame,cv2.IMREAD_COLOR)
        

                #img = cv2.cvtColor(img,cv2.COLOR_RGB2BGR) ## not necessary, default is already BGR
        
                img = cv2.normalize(img,None,0,255,cv2.NORM_MINMAX)
                #img = np.moveaxis(imh)
        
                img = cv2.resize(img,(227,227))
        
        

                img = img.astype(np.float32)
        
                img = img - mu
        
                # height, width, depth = oriimg.shape
                # imgScale = W/width
                # newX,newY = oriimg.shape[1]*imgScale, oriimg.shape[0]*imgScale
                #try:
       
        

        
        
                #except: import pdb; pdb.set_trace()
                img = np.expand_dims(img,axis = 0)
                img_data = np.append(img_data, img, axis = 0)
            i = i + 5
            #i += 1
            img = img_data

        else:
            ## load_Frame ## need to crop to 227,227
            # import cv2
            # filename = 'your_image.jpg'
            # W = 1000.
            ## check that images ar
            frame = frame_list[i]
            img = cv2.imread(frame,cv2.IMREAD_COLOR)
        
            
            #img = cv2.cvtColor(img,cv2.COLOR_RGB2BGR) ## not necessary, default is already BGR
            
            img = cv2.normalize(img,None,0,255,cv2.NORM_MINMAX)
            #img = np.moveaxis(imh)
            
            img = cv2.resize(img,(227,227))
        
        
            
            img = img.astype(np.float32)
            
            img = img - mu
        
            # height, width, depth = oriimg.shape
            # imgScale = W/width
            # newX,newY = oriimg.shape[1]*imgScale, oriimg.shape[0]*imgScale
            #try:
       
        

        
        
            #except: import pdb; pdb.set_trace()
            img = np.expand_dims(img,axis = 0)
            # cv2.imshow("Show by CV2",newimg)
            # Cv2.waitKey(0)
            # cv2.imwrite("resizeimg.jpg",newimg)
            i +=1
            
        #all_features_list= sess.run([conv1,conv2,conv3,conv4,conv5,fc6,fc7], feed_dict= {x: img, keep_prob : [1]})
        all_features_list=  sess.run([fc8], feed_dict= {x: img, keep_prob : [1]})
        for feature, feature_name in zip(all_features_list, layer_name_list):
            feature_dic[feature_name].append(feature)
    print("Saving features...")
    #import pdb; pdb.set_trace()
    
    for layer_name, feature_list in tqdm(feature_dic.items()):
        clip_name = movie_path.split('/')[-1]
        video_clip_feat_path = os.path.join(__feat_data_root__,clip_name) # /data1/amelie/EnD/alexnet/CUBE*/conv1a.feat
        if not os.path.exists(video_clip_feat_path):
            os.mkdir(video_clip_feat_path)
        feat_file = os.path.join(video_clip_feat_path, layer_name + '.feat')

        if os.path.exists(feat_file):
            print("{} already exists, not overwriting.".format(layer_name))
            continue
        else: 
            with open(feat_file, "wb") as f:
                pkl.dump(np.array(feature_list, dtype = np.float32), f, protocol = 4)
                #f.write(bytearray(np.array(feature_list,dtype = np.float32)))


### write feature for each single frame in CUBE folder, within cubee seperate by layeer

### create gathered features CUBE.feat 
