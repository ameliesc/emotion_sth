import glob
import numpy as np
import _pickle as pkl
import os
from shutil import copy2

__feat_dir__ = '/data1/amelie/EnD/alexnet/alexnet_run/fpf1'


#feat_list = ['conv1', 'conv2', 'conv3', 'conv4', 'conv5', 'fc6', 'fc7']
feat_list = ['fc8']
movie_paths = glob.glob(os.path.join(__feat_dir__, '*cropped*'))


for movie in movie_paths:
    for layer in feat_list:
        layer_path = os.path.join(__feat_dir__, layer)
        movie_feat = os.path.join(movie,layer + '.feat')
        assert os.path.exists(movie_feat)
        movie_name = movie.split('/')[-1]
        if not os.path.exists(layer_path):
            os.mkdir(layer_path)
        copy2(movie_feat, os.path.join(layer_path, movie_name + '.feat' ))
