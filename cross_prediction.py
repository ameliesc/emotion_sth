import os
import logging
logging.getLogger('tensorflow').setLevel(logging.INFO)
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
import tensorflow as tf
from sklearn.decomposition import PCA
from pathos.multiprocessing import ProcessingPool as Pool
from math import floor
from collections import OrderedDict
from tqdm import tqdm
from read_tfrecord import load_data, create_c3d_layer_dict
from p_tqdm import p_map
from sklearn.model_selection import KFold
from linear_regression3 import RidgeRegression
from model_selection import get_pearson_scores
import numpy as np
import _pickle as pkl
import scipy
from scipy import io

__train_size__ = 0.9


def load_general_data(layer,net, data_pipeline,response,fpf):
    dataset = load_data(__filename__,layer, 1, 1, __train_size__, mode = "train", net = net, data_pipeline = data_pipeline,response = response,fpf = fpf) ## removwe mode from load function, niot relevant for our dataset 
    eval_dataset = load_data(__filename__,layer, 1, 1, __train_size__, mode = "eval",  net = net, data_pipeline = data_pipeline,response =response,fpf = fpf)
    return dataset,eval_dataset



def run_data(dataset,sess):
    iterator = dataset.make_one_shot_iterator()
    next_element = iterator.get_next() #c3d_dict, main_data, mimic_data, aourasl ,valence
    c3d = next_element[0]
    
    
    
    fmri_data= next_element[1]
    
    arousal = next_element[2]
    valence =  next_element[2]
    #print("Loading kfold data...")
    c3d_list = []
    voxel_list = []
    arousal_list = []
    valence_list = []
    ##Batch size should be 1
    #sess = tf.Session()
    #sess.run(tf.initialize_all_variables())
    
    #import pdb; pdb.set_trace()
    
    while True:
        try: ## need to load data like this because we have to use scikit for crossval ... :( so slow
        ## mskr yhid s seperate function, load all voxels once, and just call different one depending on loop
            
            
            
            x, y,w,z = sess.run([c3d,fmri_data, arousal,valence])
            x.reshape([1, -1])
            
            # return to stuff
            
        
            c3d_list.append(x)
            voxel_list.append(y)
            arousal_list.append(w)
            valence_list.append(z)
        except:
            break
    c3d = np.concatenate(c3d_list, axis = 0)
    voxel1 = np.concatenate(voxel_list, axis = 0)
    
    #print("Done.")
    #sess.close()
    return np.squeeze(c3d), np.squeeze(voxel1), arousal_list,valence_list


def cross_prediction(net,data_pipeline,response,fpf,p_value,pca_mode,subject):

    __filename0__ = '/data1/amelie/EnD/tfrecord/time_series_brain_rsp/{}/subject{}'.format(pca_mode,subject)
    __filename__ = '/data1/amelie/EnD/tfrecord/time_series_brain_rsp/{}/subject{}'.format(pca_mode,subject)
    __results_dir__ = '/data1/amelie/EnD/regr_output/{}/train80/subject{}'.format(pca_mode,subject)
    __mat__ = '/data1/amelie/EnD/fmri_time_series/CUBEE%04d_ts_GM.mat' % (subject + 1)
    __train_size__ = 0.80


    
    sess = tf.Session()
    
   
    main_results_file = "{}_{}_{}_encoding_model_results_p_value_{}.mat".format(net,fpf,response,p_value)

    main_results =  scipy.io.loadmat(os.path.join(__results_dir__,main_results_file))

    if response == "main": ### load mimic dataset for crosspredictions
        filename_dims = os.path.join(__filename0__,data_pipeline + '_'+ net +'_'+ "mimic" + '_' + fpf +'_dim.txt')
        layer_dict,_ = create_c3d_layer_dict(filename_dims)
        data_set_list = []
        for layer in layer_dict: ##get x,y for each layer 
            dataset, eval_dataset = load_general_data(layer,net,data_pipeline,"mimic",fpf)
            data = run_data(eval_dataset,sess)
            data_set_list.append(data) ## list index corresponds to layer
    else:
        filename_dims = os.path.join(__filename0__,data_pipeline + '_'+ net +'_'+ "main" + '_' + fpf +'_dim.txt')
        layer_dict,_ = create_c3d_layer_dict(filename_dims)
        data_set_list = []
        for layer in layer_dict.keys(): ##get x,y for each layer 
            dataset, eval_dataset = load_general_data(layer,net,data_pipeline,"main",fpf)
            data = run_data(eval_dataset,sess)
            data_set_list.append(data) ## list index corresponds to layer
        
    

    #import pdb; pdb.set_trace()
    
    main_results_file = "{}_{}_{}_cross_prediction_results_p_value_{}".format(args.net,args.fpf,args.response,args.p_value)
    
    voxel_id = main_results['voxel_id']
    layer_id = main_results['layer_id']
    layer_pearson = []
    layer_p_value = []
    voxel_id_list = []
    layer_id_list = []
    main_results= {}
    mat = io.loadmat(__mat__)
    coord = mat['DataMat']['GMcoordinate'][0][0]
    coord_list = []
    for i in range(len(layer_dict.keys())):
        #if i != 2:
        #    continue
        session = tf.Session()
        ind = np.where(layer_id == i)
        layer = [*layer_dict.keys()][i]
        weights_filename = os.path.join(__results_dir__,'{}_{}_weights.pkl'.format(layer,response))
        weights_filename2 = os.path.join(__results_dir__,'{}_{}_weights.pkl'.format(layer,'mimic'))
        regr = RidgeRegression(layer_dict[layer], 20005,session)
        regr.graph()
        with open(weights_filename, 'rb') as f:
            weights = pkl.load(f)
        with open(weights_filename2, 'rb') as f:
            weights2 = pkl.load(f)
        #import pdb; pdb.set_trace()

        regr.W = weights
        x,y_true,arousal,valence = data_set_list[i]
        y_pred = regr.predict(x)

        #import pdb; pdb.set_trace()
        voxel_id_list.append(voxel_id[0][ind[1]])
        coord_list.append(coord[ind[1]])
        pearson_r_all, p_value_all = get_pearson_scores(y_pred[:,ind[1]],y_true[:,ind[1]])
       
        layer_pearson.append(pearson_r_all)
        layer_p_value.append(p_value_all)
    
    layer_pearson = np.array(layer_pearson)
    layer_p_value = np.array(layer_p_value)
    voxel_id = np.array(voxel_id_list)
    coord = np.array(coord_list)
    pearson = []
    p_value_list = []
    vox_id = []
    cord = []
    layer_id = []
    for i in range(len(layer_pearson)):
        
        ind2 =np.where(layer_p_value[i] < 0.01)[0]
        #import pdb; pdb.set_trace()
        
        pearson.extend(layer_pearson[i][ind2])
        p_value_list.extend(layer_p_value[i][ind2])
        layer_id.extend(np.repeat(i, len(voxel_id[i][ind2])))
        vox_id.extend(voxel_id[i][ind2])
        cord.extend(coord[i][ind2])
    #ind2 = np.where(layer_p_value < 0.001)[1]
    
    main_results['voxel_id'] = np.array(vox_id)
    main_results['layer_id'] = np.array(layer_id)
    main_results['pearson_r'] = np.array(pearson)
    main_results['p_value'] =  np.array(p_value_list)
    main_results['coord'] = np.array(cord)
    #import pdb; pdb.set_trace()
    scipy.io.savemat(os.path.join(__results_dir__,main_results_file), mdict = main_results)

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description= __doc__)
    #parser.add_argument('--voxel', type = int, required = True)
    parser.add_argument('--net', type = str, required = True, choices = ['c3d', 'alexnet'])
    parser.add_argument('--data_pipeline', type = str, required = True ,  choices = ['movie_pca', 'serialized' ])
    parser.add_argument('--response', type = str, required = True, choices = ['mimic', 'main'])
    parser.add_argument('--fpf', type = str,  choices = ['fpf15', 'fpf1'], default = '')
    parser.add_argument('--p_value', type = float, required = True)
    parser.add_argument('--pca_mode', type = str, required = True, choices = ['new_pca', 'old'])
    args = parser.parse_args()

    for subject in [0,3,58,62]:
        print("Subject",subject)
        __filename__ = '/data1/amelie/EnD/tfrecord/time_series_brain_rsp/{}/subject{}'.format(args.pca_mode,subject)
        cross_prediction(args.net, args.data_pipeline,args.response,args.fpf,args.p_value,args.pca_mode,subject )
