from scipy import io
import numpy as np
import sys
import os
import tensorflow as tf
import glob
import _pickle as pkl

from collections import OrderedDict


__data_root__ = '/data1/amelie/EnD/'


def _bytes_feature(value):
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))


def gather_feature_folder(root):
    conv_feat_path_list = sorted(glob.glob(os.path.join(root, 'conv*')))
    fc_feat_path_list = sorted(glob.glob(os.path.join(root, 'fc*')))
    feat_path_list =  conv_feat_path_list + fc_feat_path_list # + 

    return feat_path_list


def load_c3d_list(path_list):
    c3d_list = []
    print("loading c3d data")
    for path in tqdm(path_list):
        #import pdb; pdb.set_trace()

        c3d = pkl.load(open(path, 'rb'))
        c3d_list.append(c3d)
        assert c3d is not None

    #import pdb; pdb.set_trace()
    if len(c3d_list) != 72:
        import pdb
        pdb.set_trace()

    #print(c3d_list[0].shape)
    # print(c3d_list[0].dtype)
    return c3d_list


def load_labels(labels_path, subject):
    loaded = io.loadmat(labels_path)
    main_data = loaded['MainData']  # (1,64)
    mimic_data = loaded['MimicData']  # (1.64) #64 subjects
    mimic_data_single = np.transpose(np.squeeze(mimic_data)[subject])  # (48273,72) -> (72,48273)
    main_data_single = np.transpose(np.squeeze(main_data)[subject])

    return main_data_single, mimic_data_single


def load_emo_labels(labels_path, subject):

    loaded = io.loadmat(labels_path)
    matrix = loaded['behReport']
    arousal = np.squeeze(matrix[0][0][0])  # (64,72)
    valence = np.squeeze(matrix[0][0][1])  # (64,72)

    return(np.squeeze(arousal[subject]), np.squeeze(valence[subject]))


def _write_to_tf_record(feat_path_list, labels_path, subject):
    arousal_single, valence_single = load_emo_labels(labels_path, subject)
    main_data_single, mimic_data_single = load_labels(labels_path, subject)
 
    c3d_list = []

    for path in feat_path_list:
        layer_name = path.split('/')[-1]
        tfrecord_path = os.path.join(
        __data_root__, 'subject' + str(subject))
        if not os.path.exists(tfrecord_path):
            os.mkdir(tfrecord_path)
        writer = tf.python_io.TFRecordWriter(os.path.join(tfrecord_path,'{}.tfrecord'.format(layer_name)))
        print("gathering data for {}".format(layer_name))
        clip_c3ds_path_list = sorted(glob.glob(os.path.join(path, "*")))

        c3d_list = load_c3d_list(clip_c3ds_path_list)
        i = 0
        assert len(c3d_list) == 72 #72 movie clips per subject
        for c3d in c3d_list:
            ## c3d length differes per clip because of different fps nummber!! [25, n_dim] , [22, ndim]
 

            for k in range(c3d.shape[0]):
                feature = {'/input/c3d/' + layer_name:  _bytes_feature(tf.compat.as_bytes(c3d[k].tostring()))}
                   # to do: check type
                feature['/label/arousal'] =  _bytes_feature(tf.compat.as_bytes(arousal_single[i].tostring())) #np.float64
                feature['/label/valence'] = _bytes_feature(tf.compat.as_bytes(valence_single[i].tostring())) #np.float64
                feature['/label/main_data'] = _bytes_feature(tf.compat.as_bytes(main_data_single[i].tostring())) #np.float64

                feature['/label/mimic_data']= _bytes_feature(tf.compat.as_bytes(mimic_data_single[i].tostring()))  # to do: check type

                example = tf.train.Example(features=tf.train.Features(feature=feature))
                writer.write(example.SerializeToString())
            i += 1
        

        #example = tf.train.Example(features=tf.train.Features(feature=feature))
        #writer.write(example.SerializeToString())
 

        print("created '{}'.tfrecord for subject {}".format(layer_name,'subject' + str(subject)))
        writer.close()
        sys.stdout.flush()


def create_tf_record():
    feat_path_list = gather_feature_folder(os.path.join(__data_root__, 'c3d'))

    labels_path = os.path.join(__data_root__, 'Emotion_neuralData.mat')
    for subject in range(0, 64):
        print("Creating tfrecord for subject{}".format(subject))
        _write_to_tf_record(feat_path_list, labels_path, subject)


if __name__ == "__main__":
    create_tf_record()
