from tqdm import tqdm
from scipy import io
from scipy import signal
import spm_hrf
from sklearn.decomposition import PCA, IncrementalPCA
from sklearn.preprocessing import StandardScaler
from collections import OrderedDict
import tensorflow_transform as tft
import hrf_estimation
import numpy as np
import sys
import os
import tensorflow as tf
from tensorflow import keras
import glob
import _pickle as pkl

# this code should also work for alex net features provid they are organized in the same folder

__data_root__ = '/data1/amelie/EnD/'
__tfrecord_folder__ = 'tfrecord/time_series_brain_rsp/'


def create_c3d_layer_dict(dim_file):
    layer_dict = OrderedDict()

    with open(dim_file, 'r+') as f:
        lines = f.readlines()

    for i in range(1, len(lines)):  # skip first line

        layer_name, dims = lines[i].split('(')
        layer_name = layer_name[0:-1]

        dims = dims[:-1]  # get rid of "\n"
        dims = dims.split(',')
        # dims[0] = dims[0][1] #get rid of bracket
        dims[-1] = dims[-1][1:-1]
        dataset_size = dims[0]

        for i in range(len(dims)):
            dims[i] = int(dims[i])
        layer_dict[layer_name] = dims[1:]
    return layer_dict


def _bytes_feature(value):
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))


def load_mat_file(labels_path):
    return io.loadmat(labels_path)


def load_labels(subject, response):
    """
    returns touple of single subject labels , labels include fmri response, valence arousal score and movie sqeuence order for a single subject
    output:
     fmri_data: np.array (20008,240)
     valence: np.array (72)
     arousal: np.array (72)
     movie_squence: np.array (72) 
    """
    fmri_labels_path = os.path.join(os.path.join(
        __data_root__, 'fmri_time_series'), 'CUBEE%04d_ts_GM.mat' % (subject + 1))
    mat_loaded = load_mat_file(fmri_labels_path)

    if response == "main":
        # (3) -> (20005,241)
        fmri_time_series = mat_loaded['DataMat']['MV'][0][0][0]
    else:

        # (3) -> (20005,241) ## 1 is a problem, shouldn't be there
        fmri_time_series = mat_loaded['DataMat']['MVS'][0][0][0]
    arousal = mat_loaded['DataMat']['emo'][0][0][0][0][0]  # (3,26)
    valence = mat_loaded['DataMat']['emo'][0][0][0][0][1]  # (3,26)

    arousal_combined = arousal[0][1:25]  # (3,24)
    valence_combined = valence[0][1:25]
    fmri_combined = fmri_time_series[0]

    for i in range(1, 3):

        fmri_combined = np.append(fmri_combined, fmri_time_series[i], axis=1)
        arousal_combined = np.append(
            arousal_combined, arousal[i][1:25], axis=0)
        valence_combined = np.append(
            valence_combined, valence[i][1:25], axis=0)

    fmri_combined = fmri_combined.T
    assert fmri_combined.shape == (720, 20005)

    assert arousal_combined.shape[0] == 72
    return fmri_combined, arousal_combined, valence_combined


def get_c3ds_path_sorted(path, net, response):
    """sorts movies in the order giving in .mat file (movie sequence)"""
    movie_list = []

    for num in range(1, 4):

        if response == "main":
            if net == "alexnet":
                single_movie = sorted(
                    glob.glob(os.path.join(path, "Run%d*.feat" % num)))
            elif net == "c3d":
                single_movie = sorted(
                    glob.glob(os.path.join(path, "Run%d*.c3d" % num)))
        else:
            if net == "alexnet":
                single_movie = sorted(
                    glob.glob(os.path.join(path, "Ctr%d*.feat" % num)))
            elif net == "c3d":
                single_movie = sorted(
                    glob.glob(os.path.join(path, "Ctr%d*.c3d" % num)))

        movie_list.extend(single_movie)

    return movie_list


def gather_feature_folder(root, net, mode, fpf):
    """
    returns list containig path to features for each layer
    eg: ['/data1/amelie/EnD/c3d/conv1a', ...]
    """
    #import pdb; pdb.set_trace()

    if mode == "clips":
        root = os.path.join(os.path.join(root, net + '_clips'), fpf)
    else:
        root = os.path.join(os.path.join(root, net + '_run'), fpf)

    conv_feat_path_list = sorted(glob.glob(os.path.join(root, 'conv*')))
    fc_feat_path_list = sorted(glob.glob(os.path.join(root, 'fc*')))
    feat_path_list = conv_feat_path_list + fc_feat_path_list  # +

    return feat_path_list


def load_and_process_c3d(feat_path, fpf):
    """
    returns average c3d values over single movie clip

    """

    c3d = pkl.load(open(feat_path, 'rb'))  # protocol 4 for alexnet
    if fpf == 'fpf15':
        c3d = np.reshape(c3d, (c3d.shape[0], c3d.shape[1], -1))
    else:
        c3d = np.reshape(c3d, (c3d.shape[0] * c3d.shape[1], -1))
    return c3d


def apply_pca(avg_c3d_movie_list, filename, filename_mean, fpf, net, components=None):

    avg_c3d_movie_arr = np.array(avg_c3d_movie_list)

    #components = 540
    batch = 720

    if 'c3d' in net:
        pca = IncrementalPCA(n_components=components, batch_size=batch)

        for i in range(0, int(avg_c3d_movie_arr.shape[0] / batch)):
            print('fitting batch {}'.format(i))
            pca.partial_fit(avg_c3d_movie_arr[i * batch:(i + 1) * batch])

        components = pca.components_
        mean = pca.mean_

    if fpf == 'fpf15':
        # max_pool =keras.layers.GlobalMaxPooling1D()
        # avg_c3d_movie_arr = tf.constant(avg_c3d_movie_arr)
        # avg_c3d_movie_arr = max_pool(avg_c3d_movie_arr)
        # avg_c3d_movie_arr = avg_c3d_movie_arr.eval(session = 1)
        #avg_c3d_movie_arr = np.max(avg_c3d_movie_arr, axis = 1)

        if components is not None:

            # retain 99% information
            pca = PCA(n_components=components, svd_solver='auto')
            pca.fit(avg_c3d_movie_arr)
            components = pca.components_
            mean = pca.mean_

        else:

            # retain 99% information
            pca = PCA(n_components=0.98, svd_solver='full')
            pca.fit(avg_c3d_movie_arr)
            components = pca.components_
            mean = pca.mean_
    elif fpf == 'fpf1':
        if components is not None:
            # retain 99% information
            pca = PCA(n_components=components, svd_solver='auto')
            pca.fit(avg_c3d_movie_arr)
            components = pca.components_
            mean = pca.mean_
        else:

            # retain 99% information
            pca = PCA(n_components=0.98, svd_solver='full')
            pca.fit(avg_c3d_movie_arr)
            components = pca.components_
            mean = pca.mean_

    # saving as tfrecord because rahter large otherwise
    print(sum(pca.explained_variance_ratio_))
    with open(filename, 'wb') as f:
        pkl.dump(components, f, protocol=4)
    with open(filename_mean, 'wb') as f:
        pkl.dump(mean, f, protocol=4)

    c3d_movie_pca = pca.transform(avg_c3d_movie_arr)

    return c3d_movie_pca


def standardize_time_series(c3d_movie):
    scaler = StandardScaler()
    t_dim, feat_dim = c3d_movie.shape
    c3d_movie = scaler.fit_transform(c3d_movie)
    return c3d_movie


def get_run_sequence(path, mode, subject):

    # runsequence same for merged stim and main
    path1 = os.path.join(path, 'runorder.mat')
    path2 = os.path.join(path, 'runmovieorder.mat')
    run_sequence = io.loadmat(path1)

    run_order = io.loadmat(path2)

    run_sequence = run_sequence['SubRunOrder'][subject]

    run_order = run_order['select']
    movie_order = []
    for n in run_sequence:

        array = run_order[n - 1]
        array_list = array.tolist()
        for movie_id in array_list:
            movie_order.append(movie_id)

    return run_sequence, movie_order


# , convolved,hrf,feat,sess):
def _write_to_tf_record(feat_path_list, labels, subject, net_name, response, fpf, pca_mode):

    fmri_data, arousal, valence = labels

    if pca_mode == 'old':
        tfrecord_path = os.path.join(os.path.join(
            __data_root__ + __tfrecord_folder__, 'old'), 'subject' + str(subject))
        tfrecord_path2 = os.path.join(
            __data_root__ + __tfrecord_folder__, 'old')
    elif pca_mode == 'new_pca':
        tfrecord_path = os.path.join(os.path.join(
            __data_root__ + __tfrecord_folder__, 'new_pca'), 'subject' + str(subject))
        tfrecord_path2 = os.path.join(
            __data_root__ + __tfrecord_folder__, 'new_pca')

    if not os.path.exists(tfrecord_path):
        os.mkdir(tfrecord_path)
    filename = os.path.join(
        tfrecord_path, 'serialized_{}_{}_{}_dim.txt'.format(net_name, response, fpf))

    if not os.path.exists(filename):
        with open(filename, 'w') as f:
            f.write("c3d dimensions for each layer\n")

    feat_path_list.reverse()

    for path in feat_path_list:  # loop over he layers

        layer_name = path.split('/')[-1]

        # if 'conv5' not in path:
        #    continue
        tf_filename = (os.path.join(tfrecord_path, 'serialized_{}_{}_{}_{}.tfrecord'.format(
            layer_name, net_name, response, fpf)))

        pca_filename = os.path.join(tfrecord_path2, 'serialized_{}_{}_{}_{}_pca_components.pkl'.format(
            net_name, response, fpf, layer_name))
        pca_filename_mean = os.path.join(tfrecord_path2, 'serialized_{}_{}_{}_{}_pca_mean.pkl'.format(
            net_name, response, fpf, layer_name))
        if 'mat' in layer_name:
            continue
        # if 'conv5' not in layer_name:
        #    continue

        if not os.path.exists(tfrecord_path):
            os.mkdir(tfrecord_path)
        if os.path.exists(filename):

            with open(filename, 'r') as f:
                content = f.readlines()

                exists = False
                for element in content:

                    if layer_name in element:
                        exists = True
            if not exists:
                with open(filename, 'a') as f:
                    f.write("{} ".format(layer_name))
        else:
            with open(filename, 'a') as f:
                f.write("{} ".format(layer_name))
        print("gathering data for {}".format(layer_name))

        if os.path.exists(tf_filename):
            continue
        # change function below to match run order given for each subject
        clip_c3ds_path_list = get_c3ds_path_sorted(path, net_name, response)

        c3ds = load_and_process_c3d(
            clip_c3ds_path_list[0], fpf).astype(np.float32)
        for clip_c3d in tqdm(clip_c3ds_path_list[1:]):
            if '.mat' in clip_c3d:
                continue

            c3ds = np.append(c3ds, load_and_process_c3d(
                clip_c3d, fpf).astype(np.float32), axis=0)

        c3d_movie = np.array(c3ds)

        # downsampling because of memory problems
        if response == "main":

            if os.path.exists(pca_filename) and os.path.exists(pca_filename_mean):
                with open(pca_filename, 'rb') as f:
                    components = pkl.load(f)

                with open(pca_filename_mean, 'rb') as f:
                    mean = pkl.load(f)
                #import pdb; pdb.set_trace()
                if fpf == 'fpf15':
                    c3ds = np.max(c3ds, axis=1)
                # else:
                c3d = (c3ds - mean).dot(components.T)

                #c3d = np.matmul(c3ds,np.transpose(components))

            else:
                if fpf == 'fpf15':
                    c3ds = np.max(c3ds, axis=1)
                if pca_mode == 'old':
                    n_components = 1800
                elif pca_mode == 'new_pca':
                    n_components = 590

                c3d = apply_pca(c3ds, pca_filename, pca_filename_mean,
                                fpf, net_name, components=n_components)

        elif response == "mimic":
            if os.path.exists(pca_filename) and os.path.exists(pca_filename_mean):
                with open(pca_filename, 'rb') as f:
                    components = pkl.load(f)
                with open(pca_filename_mean, 'rb') as f:
                    mean = pkl.load(f)
                if fpf == 'fpf15':
                    c3ds = np.max(c3ds, axis=1)
                c3d = (c3ds - mean).dot(components.T)
            else:
                if fpf == 'fpf15':
                    c3ds = np.max(c3ds, axis=1)
                filename_dims = os.path.join(
                    tfrecord_path, 'serialized_{}_{}_{}_dim.txt'.format(net_name, "main", fpf))
                #import pdb; pdb.set_trace()

                layer_dict = create_c3d_layer_dict(filename_dims)
                # n_components = 540 #layer_dict[layer_name]
                #import pdb; pdb.set_trace()
                if pca_mode == 'old':
                    n_components = 1800
                elif pca_mode == 'new_pca':
                    n_components = 590

                c3d = apply_pca(c3ds, pca_filename, pca_filename_mean,
                                fpf, net_name, components=n_components)
        c3d = standardize_time_series(c3d)

        #import pdb; pdb.set_trace()

        #####
        # convolving with predefinded hrf, and downsampling to mach sample rate of fmri (1.5 Hz)
        #####
        if fpf == 'fpf1':

            srate = 30  # 30 frames per second
        elif fpf == 'fpf1-2':
            srate = 2
        elif fpf == 'fpf15':
            srate = 6  # 1

        # concolving with hrf
        #p  = np.array([5, 16, 1, 1, 6, 0, 32])
        #hrf_est = spm_hrf.spm_hrf(1/srate, p)
        #hrf_est = np.expand_dims(hrf_est,axis=1)
        #ts = signal.fftconvolve(hrf_est,c3d)
        #ts = ts[(6*srate)-1:6*srate+c3d.shape[0],:]
        # downsampling
        ts = c3d 
        ts = ts[srate - 1::int(1.5 * srate), :]  # downsampling to match fMRI
        c3d_down_sample = ts
        
        io.savemat(os.path.join(tfrecord_path,layer_name + '_down_sampled.mat'), {layer_name : c3d_down_sample})
        assert c3d_down_sample.shape[0] == fmri_data.shape[0]
        # Downsampling

        with open(filename, 'a') as f:
            f.write('{}\n'.format(c3d_down_sample.shape))
        writer = tf.python_io.TFRecordWriter(os.path.join(
            tfrecord_path, 'serialized_{}_{}_{}_{}.tfrecord'.format(layer_name, net_name, response, fpf)))

        j = 0
        for k in range(c3d_down_sample.shape[0]):

            feature = {'/input/c3d/' + layer_name:  _bytes_feature(
                tf.compat.as_bytes(c3d_down_sample[k].tostring()))}
            # float32 -> alexnet
            # int16
            feature['/label/arousal'] = _bytes_feature(
                tf.compat.as_bytes(arousal[j].tostring()))
            # int16
            feature['/label/valence'] = _bytes_feature(
                tf.compat.as_bytes(valence[j].tostring()))

            feature['/label/{}_data'.format(response)] = _bytes_feature(
                tf.compat.as_bytes(fmri_data[k].astype(np.float32).tostring()))  # float32
            if k % 10 == 0 and k > 9:
                j += 1
            example = tf.train.Example(
                features=tf.train.Features(feature=feature))
            writer.write(example.SerializeToString())

        assert j == 71

        print("created '{}_{}'.tfrecord for subject {}".format(
            layer_name, net_name, str(subject)))
        writer.close()
        sys.stdout.flush()


def create_tf_record(net_name, response, fpf, pca_mode):
    mode = 'time_series'
    # if 'fpf15' in fpf:
    #    fpf2 = 'fpf15'
    feat_path_list = gather_feature_folder(os.path.join(
        __data_root__, net_name), net_name, mode, fpf)

    for subject in range(0, 107):

        if subject not in [0, 3, 14, 58, 62]:  # ,62,50,0,14,18,89,8,106]:
            continue
        # if subject == 3: ## matlab file for 3 is empty!!! no fmri records!!!
        #    continue
        print("Creating tfrecord for subject{}".format(subject))
        labels = load_labels(subject, response)
        _write_to_tf_record(feat_path_list, labels, subject, net_name,
                            response, fpf, pca_mode)  # ,convolved,hrf,feat,sess)


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('--net', type=str,
                        choices=['alexnet', 'c3d'], required=True)
    parser.add_argument('--response', type=str, choices=['mimic', 'main'])
    parser.add_argument('--fpf', type=str,
                        choices=['fpf15', 'fpf1', '15fpf', 'fpf1-2'])
    parser.add_argument('--pca_mode', type=str, choices=['old', 'new_pca'])
    args = parser.parse_args()
    # args : c3d, alexnet
    # change name of tfrecord accordingky
    create_tf_record(args.net, args.response, args.fpf, args.pca_mode)
