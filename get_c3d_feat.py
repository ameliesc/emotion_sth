import cv2
from PIL import Image
import numpy as np
import os
from scipy.sparse import coo_matrix, issparse
import sys
import subprocess
import _pickle as pkl
import glob
import collections
import array
import numpy as np
import h5py
from shutil import copyfile
import logging
#import skimage
#from skimage.transform import resize
#import skimage.color as color

#logging.Logger.setLevel(level = )

__c3d_length__ = 512

def read_binary_blob(filename):

    read_status = 1
    blob = collections.namedtuple('Blob', ['size', 'data'])

    f = open(filename, 'rb')
    s = array.array("i") # int32
    s.fromfile(f, 5)

    if len(s) == 5 :
        m = s[0]*s[1]*s[2]*s[3]*s[4]

        # [data, c] = fread(f, [1 m], precision)
        data_aux = array.array("f")
        data_aux.fromfile(f, m)
        data = np.array(data_aux.tolist())

        if len(data) != m:
            read_status = 0;

    else:
        read_status = 0;

    # If failed to read, set empty output and return
    if not read_status:
        s = []
        blob_data = []
        b = blob(s, blob_data)
        return s, b, read_status

    # reshape the data buffer to blob
    # note that MATLAB use column order, while C3D uses row-order
    # blob = zeros(s(1), s(2), s(3), s(4), s(5), Float);
    blob_data = np.zeros((s[0], s[1], s[2], s[3], s[4]), np.float32)
    off = 0
    image_size = s[3]*s[4]
    for n in range(0, s[0]):
        for c in range(0, s[1]):
            for l in range(0, s[2]):
                # print n, c, l, off, off+image_size
                tmp = data[np.array(range(off, off+image_size))];
                blob_data[n][c][l][:][:] = tmp.reshape(s[3], -1);
                off = off+image_size;


    b = blob(s, blob_data)
    f.close()
    return s, b, read_status

gpu_id = 0

batch_size = 50
__force_computing__ = True
__video_width__ = 400.0 
caffe_root = "/data/amelie/C3D/C3D-v1.0/"


def check_trained_model(trained_model):
    """Checks if the trained_model is there, otherwise it gets downloaded"""

    if os.path.isfile(trained_model):
        logging.info(" trainded_model={} found. Continuing... ".format(trained_model))
    else:
        donwload_cmd = [
            "wget",
            "-0",
            trained_model,
            "https://www.dropbox.com/s/vr8ckp0pxgbldhs/conv3d_deepnetA_sport1m_iter_1900000?dl=0",
            ]

        logging.info(" Download sports1m pre-trained model: \"{}\"".format(' '.join(download_cmd)
        ))

        return_code  = subprocess.call(download_cmd) # calls command in command line

        if return_code != 0:
            print("[Error] Downloading of pretrained model failed. Check!")
            sys.exit(-10)

    return


# def get_frame_count(video):
#     """Get frame count and FPS for a single video clip """

    
#     cap = cv2.VideoCapture(video)
#     if not cap.isOpened():
#         print("[Error] video={} cannot be opened.".format(video))
#         sys.exit(-6) #termiante python with exit status -6

#     num_frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
   

#     fps = cap.get(cv2.CAP_PROP_FPS) #frames per second
#     if not fps or fps != fps:
#         fps = 29.97

#     return num_frames, fps

# def extract_frames(video, frame_dir,num_frames, num_frames_to_extract=16):
#     # check output directory
#     video_id = video.split('/')[-1].split('.')[0]
#     frame_dir = os.path.join(frame_dir, video_id)
#     if os.path.isdir(frame_dir):
#         logging.warning(" frame_dir={} does exist. Will overwrite".format(frame_dir))
#     else:
#         os.makedirs(frame_dir)

#     cap = cv2.VideoCapture(video) #returns video_capture object
#     if not cap.isOpened():
#         print ("[Error] video={} can not be opened.".format(video))
#         sys.exit(-6)
#     global frames_n
#    # cap.set(cv2.CAP_PROP_POS_FRAMES, start_frame)
#     height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
#     width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
#     scale = __video_width__ / width
#     new_height = int(scale * height)
#     new_width = int(scale * width)
#     frames_n = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
    
#     #get frames and save
#     #for frame_count in range(0,(num_frames - num_frames_to_extract), num_frames_to_extract):
#     for frame_count in range( num_frames):
        
    
#         frame_num = frame_count #+ start_frame
#         logging.info("Extracting frame num={}".format(frame_num))
#         ret, frame = cap.read()
#         if not ret:
#             print("[Error] Frame extraction was not successful")
#             sys.exit(-7)
        
        
#         frame = cv2.resize(frame, (new_width,new_height))
#         frame_file = os.path.join(frame_dir, '{0:06d}.jpg'.format(int(frame_num) + 1))
#         #if frame_num%16 ==0:
#         cv2.imwrite(frame_file, frame)
#     # if len(glob.glob("%s/*.jpg" % frame_dir)) != frames_n+1:
#     #     print("===============", frames_n, "===============")
#     #     cnt = 1
#     #     while True:
#     #         success, frame = cap.read()
#     #         if not success:
#     #             break
#     #         frame = cv2.resize(frame, (new_width,new_height))
#     #         cv2.imwrite("%s/%06d.jpg" % (frame_dir,cnt), frame)
#     #         cnt += 1
#      #   cap.release()

#     # return

# def process_frame(video, start_frame, frame_dir,num_frames, num_frames_to_extract=16):
#     # check output directory
#     video_id = video.split('/')[-1].split('.')[0]
#     frame_dir = os.path.join(frame_dir, video_id)
#     if os.path.isdir(frame_dir):
#         logging.warning(" frame_dir={} does exist. Will overwrite".format(frame_dir))
#     else:
#         os.makedirs(frame_dir)

#     cap = cv2.VideoCapture(video) #returns video_capture object
#     if not cap.isOpened():
#         print ("[Error] video={} can not be opened.".format(video))
#         sys.exit(-6)
#     global frames_n
#    # cap.set(cv2.CAP_PROP_POS_FRAMES, start_frame)
#     height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
#     width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
#     scale = __video_width__ / width
#     new_height = int(scale * height)
#     new_width = int(scale * width)
#     frames_n = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
    
#     #get frames and save
#     #for frame_count in range(0,(num_frames - num_frames_to_extract), num_frames_to_extract):
#     for frame_count in range( num_frames):
        
    
#         frame_num = frame_count #+ start_frame
#         logging.info("Extracting frame num={}".format(frame_num))
#         ret, frame = cap.read()
#         if not ret:
#             print("[Error] Frame extraction was not successful")
#             sys.exit(-7)
        
        
#         frame = cv2.resize(frame, (new_width,new_height))
#         frame_file = os.path.join(frame_dir, '{0:06d}.jpg'.format(int(frame_num) + 1))
#         #if frame_num%16 ==0:
#         cv2.imwrite(frame_file, frame)
#     # if len(glob.glob("%s/*.jpg" % frame_dir)) != frames_n+1:
#     #     print("===============", frames_n, "===============")
#     #     cnt = 1
#     #     while True:
#     #         success, frame = cap.read()
#     #         if not success:
#     #             break
#     #         frame = cv2.resize(frame, (new_width,new_height))
#     #         cv2.imwrite("%s/%06d.jpg" % (frame_dir,cnt), frame)
#     #         cnt += 1
#      #   cap.release()

#     # return


def generate_feature_prototxt(out_file, src_file, mean_file=None): # what is mean_file? -> sport1m_train16_128_mean.binaryproto
    """Generate a model architeture, pointing to the given src_file """
    
    if not mean_file:
        mean_file = os.path.join(
            caffe_root,
            "examples",
            "c3d_feature_extraction",
            "sport1m_train16_128_mean.binaryproto")  ##TODO caffe root print later
    if not os.path.isfile(mean_file):
        print("[Error] mean cube file={} does not exist.".format(mean_file))
        sys.exit(-8)
   
    
    prototxt_content = '''
name: "DeepConv3DNet_Sport1M_Val"
layers {{
  name: "data"
  type: VIDEO_DATA
  top: "data"
  top: "label"
  image_data_param {{
    source: "{0}"
    use_image: true
    mean_file: "{1}"
    batch_size: {2}
    crop_size: 112
    mirror: false
    show_data: 0
    new_height: 128
    new_width: 171
    new_length: 16
    shuffle: false
  }}
}}
# ----------- 1st layer group ---------------
layers {{
  name: "conv1a"
  type: CONVOLUTION3D
  bottom: "data"
  top: "conv1a"
  blobs_lr: 1
  blobs_lr: 2
  weight_decay: 1
  weight_decay: 0
  convolution_param {{
    num_output: 64
    kernel_size: 3
    kernel_depth: 3
    pad: 1
    temporal_pad: 1
    weight_filler {{
      type: "gaussian"
      std: 0.01
    }}
    bias_filler {{
      type: "constant"
      value: 0
    }}
  }}
}}
layers {{
  name: "relu1a"
  type: RELU
  bottom: "conv1a"
  top: "conv1a"
}}
layers {{
  name: "pool1"
  type: POOLING3D
  bottom: "conv1a"
  top: "pool1"
  pooling_param {{
    pool: MAX
    kernel_size: 2
    kernel_depth: 1
    stride: 2
    temporal_stride: 1
  }}
}}
# ------------- 2nd layer group --------------
layers {{
  name: "conv2a"
  type: CONVOLUTION3D
  bottom: "pool1"
  top: "conv2a"
  blobs_lr: 1
  blobs_lr: 2
  weight_decay: 1
  weight_decay: 0
  convolution_param {{
    num_output: 128
    kernel_size: 3
    kernel_depth: 3
    pad: 1
    temporal_pad: 1
    weight_filler {{
      type: "gaussian"
      std: 0.01
    }}
    bias_filler {{
      type: "constant"
      value: 1
    }}
  }}
}}
layers {{
  name: "relu2a"
  type: RELU
  bottom: "conv2a"
  top: "conv2a"
}}
layers {{
  name: "pool2"
  type: POOLING3D
  bottom: "conv2a"
  top: "pool2"
  pooling_param {{
    pool: MAX
    kernel_size: 2
    kernel_depth: 2
    stride: 2
    temporal_stride: 2
  }}
}}
# ----------------- 3rd layer group --------------
layers {{
  name: "conv3a"
  type: CONVOLUTION3D
  bottom: "pool2"
  top: "conv3a"
  blobs_lr: 1
  blobs_lr: 2
  weight_decay: 1
  weight_decay: 0
  convolution_param {{
    num_output: 256
    kernel_size: 3
    kernel_depth: 3
    pad: 1
    temporal_pad: 1
    weight_filler {{
      type: "gaussian"
      std: 0.01
    }}
    bias_filler {{
      type: "constant"
      value: 1
    }}
  }}
}}
layers {{
  name: "relu3a"
  type: RELU
  bottom: "conv3a"
  top: "conv3a"
}}
layers {{
  name: "conv3b"
  type: CONVOLUTION3D
  bottom: "conv3a"
  top: "conv3b"
  blobs_lr: 1
  blobs_lr: 2
  weight_decay: 1
  weight_decay: 0
  convolution_param {{
    num_output: 256
    kernel_size: 3
    kernel_depth: 3
    pad: 1
    temporal_pad: 1
    weight_filler {{
      type: "gaussian"
      std: 0.01
    }}
    bias_filler {{
      type: "constant"
      value: 1
    }}
  }}
}}
layers {{
  name: "relu3b"
  type: RELU
  bottom: "conv3b"
  top: "conv3b"
}}
layers {{
  name: "pool3"
  type: POOLING3D
  bottom: "conv3b"
  top: "pool3"
  pooling_param {{
    pool: MAX
    kernel_size: 2
    kernel_depth: 2
    stride: 2
    temporal_stride: 2
  }}
}}

# --------- 4th layer group
layers {{
  name: "conv4a"
  type: CONVOLUTION3D
  bottom: "pool3"
  top: "conv4a"
  blobs_lr: 1
  blobs_lr: 2
  weight_decay: 1
  weight_decay: 0
  convolution_param {{
    num_output: 512
    kernel_size: 3
    kernel_depth: 3
    pad: 1
    temporal_pad: 1
    weight_filler {{
      type: "gaussian"
      std: 0.01
    }}
    bias_filler {{
      type: "constant"
      value: 1
    }}
  }}
}}
layers {{
  name: "relu4a"
  type: RELU
  bottom: "conv4a"
  top: "conv4a"
}}
layers {{
  name: "conv4b"
  type: CONVOLUTION3D
  bottom: "conv4a"
  top: "conv4b"
  blobs_lr: 1
  blobs_lr: 2
  weight_decay: 1
  weight_decay: 0
  convolution_param {{
    num_output: 512
    kernel_size: 3
    kernel_depth: 3
    pad: 1
    temporal_pad: 1
    weight_filler {{
      type: "gaussian"
      std: 0.01
    }}
    bias_filler {{
      type: "constant"
      value: 1
    }}
  }}
}}
layers {{
  name: "relu4b"
  type: RELU
  bottom: "conv4b"
  top: "conv4b"
}}
layers {{
  name: "pool4"
  type: POOLING3D
  bottom: "conv4b"
  top: "pool4"
  pooling_param {{
    pool: MAX
    kernel_size: 2
    kernel_depth: 2
    stride: 2
    temporal_stride: 2
  }}
}}

# --------------- 5th layer group --------
layers {{
  name: "conv5a"
  type: CONVOLUTION3D
  bottom: "pool4"
  top: "conv5a"
  blobs_lr: 1
  blobs_lr: 2
  weight_decay: 1
  weight_decay: 0
  convolution_param {{
    num_output: 512
    kernel_size: 3
    kernel_depth: 3
    pad: 1
    temporal_pad: 1
    weight_filler {{
      type: "gaussian"
      std: 0.01
    }}
    bias_filler {{
      type: "constant"
      value: 1
    }}
  }}
}}
layers {{
  name: "relu5a"
  type: RELU
  bottom: "conv5a"
  top: "conv5a"
}}
layers {{
  name: "conv5b"
  type: CONVOLUTION3D
  bottom: "conv5a"
  top: "conv5b"
  blobs_lr: 1
  blobs_lr: 2
  weight_decay: 1
  weight_decay: 0
  convolution_param {{
    num_output: 512
    kernel_size: 3
    kernel_depth: 3
    pad: 1
    temporal_pad: 1
    weight_filler {{
      type: "gaussian"
      std: 0.01
    }}
    bias_filler {{
      type: "constant"
      value: 1
    }}
  }}
}}
layers {{
  name: "relu5b"
  type: RELU
  bottom: "conv5b"
  top: "conv5b"
}}

layers {{
  name: "pool5"
  type: POOLING3D
  bottom: "conv5b"
  top: "pool5"
  pooling_param {{
    pool: MAX
    kernel_size: 2
    kernel_depth: 2
    stride: 2
    temporal_stride: 2
  }}
}}
# ---------------- fc layers -------------
layers {{
  name: "fc6-1"
  type: INNER_PRODUCT
  bottom: "pool5"
  top: "fc6-1"
  blobs_lr: 1
  blobs_lr: 2
  weight_decay: 1
  weight_decay: 0
  inner_product_param {{
    num_output: 4096
    weight_filler {{
      type: "gaussian"
      std: 0.005
    }}
    bias_filler {{
      type: "constant"
      value: 1
    }}
  }}
}}
layers {{
  name: "relu6"
  type: RELU
  bottom: "fc6-1"
  top: "fc6-1"
}}
layers {{
  name: "drop6"
  type: DROPOUT
  bottom: "fc6-1"
  top: "fc6-1"
  dropout_param {{
    dropout_ratio: 0.5
  }}
}}
layers {{
  name: "fc7-1"
  type: INNER_PRODUCT
  bottom: "fc6-1"
  top: "fc7-1"
  blobs_lr: 1
  blobs_lr: 2
  weight_decay: 1
  weight_decay: 0
  inner_product_param {{
    num_output: 4096
    weight_filler {{
    type: "gaussian"
      std: 0.005
    }}
    bias_filler {{
      type: "constant"
      value: 1
    }}
  }}
}}
layers {{
  name: "relu7"
  type: RELU
  bottom: "fc7-1"
  top: "fc7-1"
}}
layers {{
  name: "drop7"
  type: DROPOUT
  bottom: "fc7-1"
  top: "fc7-1"
  dropout_param {{
    dropout_ratio: 0.5
  }}
}}
layers {{
  name: "fc8-1"
  type: INNER_PRODUCT
  bottom: "fc7-1"
  top: "fc8-1"
  blobs_lr: 1
  blobs_lr: 2
  weight_decay: 1
  weight_decay: 0
  inner_product_param {{
    num_output: 487
    weight_filler {{
      type: "gaussian"
      std: 0.01
    }}
    bias_filler {{
      type: "constant"
      value: 0
    }}
  }}
}}
layers {{
  name: "prob"
  type: SOFTMAX
  bottom: "fc8-1"
  top: "prob"
}}
layers {{
  name: "accuracy"
  type: ACCURACY
  bottom: "prob"
  bottom: "label"
  top: "accuracy"
  #top: "prediction_truth"
}}'''.format(src_file, mean_file, batch_size)

    with open(out_file, 'w') as f:
        f.write(prototxt_content)

    return

def create_src_and_output_file(video_id,frames_n, directory,frame_dir, feat_dir,feature_layer):

    
    
    input_file = os.path.join(directory, 'input.txt')
    f_output_prefix_file = os.path.join(directory, 'output_prefix.txt')
    if os.path.isfile(input_file):
        f_input = open(input_file, 'a')
    else:
        f_input = open(input_file, 'w+')

    if os.path.isfile(f_output_prefix_file):
        f_output_prefix = open(f_output_prefix_file, 'a')
    else:
        f_output_prefix = open(f_output_prefix_file, 'w+')

    frame_dir = os.path.join(frame_dir,video_id )
    dummy_label = 0
    feat_dir = os.path.join(feat_dir,feature_layer)
    if not os.path.exists(feat_dir):
        os.mkdir(feat_dir)
    start_frame = 1
    total = 0
    
    
    while start_frame + 15 <= frames_n - 15+2:  #615 frames in a folder, last 15 frames are fake frames because for some strange reason this feature extraction fials at opening the last 15 frames, no matter how many of them are in  afolder??#frames_n:
        total +=1
        
        f_input.write("{}/ {:d} {:d} \n".format(frame_dir,int(start_frame), int(dummy_label)))
        clip_id = feat_dir + '/{0:06d}'.format(int(start_frame))
        start_frame +=15   ### exttract 1 frame per second 
        f_output_prefix.write("{}\n".format(clip_id)) # ToDo : make extra output directory
   
    
    f_input.close()
    f_output_prefix.close()
    
    return f_output_prefix_file, input_file, total




def run_C3D_extraction(feature_prototxt, ofile, feature_layer, trained_model, num_frames):
    """Extract C3D features by running caffee binary """
    num_batches = (num_frames+batch_size-1) / batch_size
    #almost_infinite_num = 9999999

    
    extract_bin = os.path.join(
            caffe_root,
            "build/tools/extract_image_features.bin"
            )

    if not os.path.isfile(extract_bin):
        print("[Error] Build facebook/C3D first, or make sure caffe_dir is "
              " correct")
        sys.exit(-9)

    feature_extraction_cmd = [
            extract_bin,
            feature_prototxt,
            trained_model,
            str(gpu_id),
            str(batch_size),
            str(num_batches),
            #str(almost_infinite_num),
            ofile,
            feature_layer,
            ]


    

    logging.info(" Running C3D feature extraction: \"{}\"".format(
            ' '.join(feature_extraction_cmd)
            ))
    return_code = subprocess.call(feature_extraction_cmd)

    return return_code

      

def process_c3d_features(feature_dir, c3d_layer, c3d_length):## read binary and save as np array

    
    def read_binary(binary_feat):
        fin = open(binary_feat, 'rb')
        s = fin.read(20)
        length = struct.unpack('i', s[4:8])[0]
        feature = fin.read(4*length)
        feature = struct.unpack("f"*length, feature)
        return list(feature)
    
    video_id = os.path.split(feature_dir)[1]

    
    logging.info( " Collecting c3d features:{}".format(video_id))

    
    video_dir = feature_dir
    

    video_dir, video_name = os.path.split(video_dir)
    
    #feat_dir = feature_dir
    #if (not __force_computing__) and os.path.isfile(feature_dir+"/"+video_name+".c3d"):
     #   continue
    binary_features = sorted(glob.glob("{}/{}/*".format(feature_dir,c3d_layer)))
    #binary_features = sorted(glob.glob("{}/*/*.{}".format(video_dir,c3d_layer)))
    feats_movie = []
    for binary_feat in binary_features:
        tmp_feat = read_binary_blob(binary_feat)[1][1]
        feats_movie.append(tmp_feat)

    if not feats_movie:
            feats_movie.append([0]*c3d_length)
           # print >> sys.stderr, "warning: %s is too short; dummy c3d features are used" % v
    # if not feats_movie:
    #    logging.warning(" {} is too short; dummy c3d features are used".format(video_id))
    #    temp = np.array(feats_movie, dtype=np.float32)
    #    samples = np.round(np.linspace(0, len(temp) - 1, 64))
    #    temp = temp[samples]
       
    #    np.save(temp, feat_dir+"/"+video_name+"_c3d.npy")
    video_dir = video_dir +'/'+ c3d_layer
    if not os.path.exists(video_dir):
        os.mkdir(video_dir)
    
    with open(video_dir  + '/'+ video_id + ".c3d", "wb") as f:
        pkl.dump(np.array(feats_movie,dtype = np.float32), f, protocol = 4) # protocol 2 so its compatible with python



def main(video_id,frame_directory,feat_dir, directory = None , input_output_file = None, trained_model = None,mean_file = None, use_attention = False, attention_mode = 'feature', feature_layer = "conv5_b", c3d_length = 512):
    
    
    ### creating new directory where pretrained model and binary file are saved

    model_files_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'c3d_model_files')
    if not os.path.exists(model_files_dir):
        os.mkdir(model_files_dir)
    new_path_trained =  os.path.join(model_files_dir,  "conv3d_deepnetA_sport1m_iter_1900000")
    new_path_binary =  os.path.join(model_files_dir,  "sport1m_train16_128_mean.binaryproto")

    if directory is None:
        directory  = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'c3d_prototxt')
        
        
        if not os.path.exists(directory):
            os.mkdir(directory)

    if trained_model is None and  not os.path.exists(new_path_trained ):
        #fined trained_model 
        trained_model = os.path.join(
        caffe_root,
        "examples",
        "c3d_feature_extraction",
        "conv3d_deepnetA_sport1m_iter_1900000"
            )
        #move trained model
        if not os.path.exists(new_path_trained ):
            copyfile(trained_model, new_path_trained)
        trained_model = new_path_trained

    else:
        if not os.path.exists(new_path_trained):
            copyfile(trained_model, new_path_trained)
        trained_model = new_path_trained
        
   
        
    if mean_file is None and not os.path.exists(new_path_binary):
        mean_file = os.path.join(
            caffe_root,
            "examples",
            "c3d_feature_extraction",
            "sport1m_train16_128_mean.binaryproto")

        if not os.path.isfile(mean_file):
            print( "[Error] mean cube file={} does not exist.".format(mean_file))
            sys.exit(-8)
        if not os.path.exists(new_path_binary):
            copyfile(mean_file, new_path_binary)
        mean_file = new_path_binary
    else:
        if not os.path.exists(new_path_binary):
            copyfile(mean_file, new_path_binary)
        mean_file = new_path_binary

   # num_frames_per_clip = 16 # ~0.5 second
    force_overwrite = False
    feat_dir = feat_dir
    #frame_dir = frame_directory  ## == video directory 
    #gazemap_dir = '/data1/amelie/Hollywood2/gazemap_cowork/'
    num_frames = len(glob.glob(os.path.join(frame_directory,video_id + "/*")))

    #import pdb; pdb.set_trace()

    
    #if not use_attention: # avoid extracting frames double
    #extract_frames(video_id, frame_dir, num_frames)
    
    ### after hxere I need to insert multiplying it with attention!!!
    
    #if not os.path.exists(os.path.join(frame_directory, video_id)):
    #    os.mkdir(os.path.join(frame_directory, video_id))

    if not os.path.exists(feat_dir):
        os.mkdir(feat_dir)
    #if not os.path.exists(os.path.join(feat_dir, video_id)):
    #        os.mkdir(os.path.join(feat_dir, video_id))
    

    # if use_attention:
        

    #     #import pdb; pdb.set_trace()
        
    #     add_attention(os.path.join(video_directory, video_id), gazemap_dir, feat_dir)
    #     frame_dir = os.path.join(frame_dir, 'with_attention')
    #     feat_dir = os.path.join(feat_dir, 'with_attention')
    #     if not os.path.exists(video_directory):
    #         os.mkdir(video_directory)
    #     if not os.path.exists(feat_dir):
    #         os.mkdir(feat_dir)

    if input_output_file is None:
        input_file = os.path.join(directory,'input.txt')
        output_file = os.path.join(directory, 'output_prefix.txt')
        if os.path.exists(input_file):
            os.remove(input_file)
        if os.path.exists(output_file):
            os.remove(output_file)

            
        output_file, input_file, total = create_src_and_output_file(video_id,num_frames, directory,frame_directory, feat_dir,feature_layer)
      #  output_file, input_file = create_src_and_output_file(video_id,start_frames, directory,frame_dir, feat_dir)
    else:
        input_file = input_output_file[0]
        output_file = input_output_file[1]


    #video_id = video_id.split('/')[-1]
    #frame_dir = os.path.join(frame_directory, video_id)
    ### i think above directory split is a little wrong- needs to be fixed cause video_ ii is alreadt
    
    feature_prototxt = os.path.join(directory,'feature_extration.prototxt')
    generate_feature_prototxt(feature_prototxt, input_file, mean_file)

    #for start_frame in start_frames:

    #import pdb; pdb.set_trace()

    if os.path.isfile(input_file) and os.path.getsize(input_file):

        
        return_code = run_C3D_extraction( ## also something going wrong here
            feature_prototxt,
            output_file,
            feature_layer,
            trained_model,
            total
            )

    ##video list get it with glob, but need to adjust above code to process multiple videos   
    
 
    
    
    
    process_c3d_features(feat_dir, feature_layer, c3d_length) 

    
if __name__ == "__main__":
    import argparse
    # To Do: add feat_dir, and frame_dir as argument 
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('--vid_file', required = True, type = str)
    parser.add_argument('--frm_directory', required = True, type = str)
    parser.add_argument('--src_file', required = False, type = str)
    parser.add_argument('--output_prefix_file', required = False, type = str)
    parser.add_argument('--prefix_file_dir', required = False, type = str)
    parser.add_argument('--trained_model', required = False, type = str)
    parser.add_argument('--mean_file', required = False, type = str)
  #  parser.add_argument('--use_attention',required = False,  action='store_true', default = False)
    parser.add_argument('--attention_mode', required = False, type = str)
    parser.add_argument('--feature_layer', required = False, type = str)
    parser.add_argument('--verbosity', required =False, type = int , default = 30)
    parser.add_argument('--feat_dir', required=True, type=str)
    args = parser.parse_args()

    main(args.vid_file,args.frm_directory,args.feat_dir)

